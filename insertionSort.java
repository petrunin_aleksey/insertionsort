package ru.pae.insertionSort;

import java.util.Arrays;

public class insertionSort {
    public static void main(String[] args) {
        int [] array = {10,3,2,4,5,1,6,7,8,9};
        System.out.println(Arrays.toString(array));
        InsertionSort(array);
        System.out.println(Arrays.toString(array));
    }

    private static void InsertionSort(int [] array) {
        for (int out = 1; out < array.length; out++) {
            int temp = array [out];
            int in = out;
            while (in > 0 && array[in - 1] >= temp) {
                array[in] = array[in - 1];
                in--;
            }
            array[in] = temp;

        }
    }
}
